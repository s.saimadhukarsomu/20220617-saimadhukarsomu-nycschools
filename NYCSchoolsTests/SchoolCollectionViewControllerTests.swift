//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Sai Madhukar Somu on 6/17/22.
//

import XCTest
@testable import NYCSchools

class SchoolCollectionViewControllerTests: XCTestCase {
    
    private struct Constants {
        
        static var school = School(dbn: "2345", name: "Test High School", phone: "8623168067", address1: "Sussex, Harrison", city: "Harrison", zip: "07029", state: "NJ", borough: "Hudson", latitude: "0.343434", longitude: "-0.34324234")
    }
    
    func testSchoolsCollection() {
        
        let layout = UICollectionViewLayout()
        let vc = SchoolsCollectionViewController(collectionViewLayout: layout)
        
        XCTAssertNotNil(vc.viewModel)
        XCTAssertNotNil(vc.schools)
        
        let detailedVC = SchoolsDetailedViewController()
        detailedVC.viewModel = vc.viewModel
        detailedVC.school = Constants.school
        
        XCTAssertNotNil(detailedVC.viewModel)
        XCTAssertNotNil(detailedVC.school)
    }
}
